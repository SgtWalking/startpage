# startpage

This project is only really meant as my startpage for dialy browsing.

It's built with `ssg5`, a script that compiles Markdown (or HTML) into HTML files using a `_header.html` and `_footer.html` to create the full site.

## build
`ssg5 src dst 'startpage' 'http://www'`
