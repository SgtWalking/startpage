# fish aliases

## config files
alias | description
--- | ---
`fishrc` | `fish` config file
`fishal` | `fish` alias file
`bspwmrc` | `bspwm` config file
`sxhkdrc` | `sxhkd` config file
`polybarrc` | `polybar` config file

## NixOS packages
alias | description
--- | ---
`inst [name]` | Install a package from the stable or unstable packages channel
`uninst [name]` | Search for a package and uninstall it

## Misc
alias | description
--- | ---
`ll` | List files as list and ignore `./..`
`get_ip` | Public IP address
`cp/rm -i` | Confirm before overwriting/deleting
`feh -d -.` | Show file names and fit image to screen

## Functions
alias | description
--- | ---
`filename` | Get file name without the extension (used in other functions)
`webp_to_jpg` | Convert webp to png/jpg using `dwebp`
`webm_to_mp3` | Convert webm to mp3 using `ffmpeg`
`imgopt` | Optimize images (and gifs) using `imagemagick`
`rename_files` | Rename directories and files to be lowercase and space-less
`vid_dir` | Make `vid` directory and move every video into it
`pic2pdf` | Print all images in a directory into a `.pdf` file
`imgrand` | Get the path of a random image in a directory and its children
`vidrand` | Get the path of a random video in a directory and its children
`tagrand` | Get the path of a random tagged image or video from `~/.local/share/ranger/tagged`
`paperand` | Set wallpaper to a random image in the `~/pic/wals` directory

## Git
alias | description
--- | ---
`gst` | `git status`
`gpu` | `git pull`
`gaa` | `git add .`
`gpm` | `git push origin master`
`gcm` | `git commit [head] [comment]`
`gcl` | `git clone [repo]`

## youtube-dl
alias | description
--- | ---
`ytdl` | Alias for `youtube-dl`
`ytda` | Extract audio from a video
`ytda_pl` | Extract audio from video playlist
`ytdv` | Download video (720p) and audio

## gallery-dl
alias | description
--- | ---
`gd` | Alias for `gallery-dl -R -1` (retry forever)
`gdi` | Get links to download from file (`~/pic/gallery-dl.txt`)
`gdd [year] [month] [day] [link]` | Apply date filter

## transmission
alias | description
--- | ---
`tcli` | Alias for `transmission-cli`
`trdm` | Start `transmission` daemon
`tremc` | Start `ncurses` client
`tradd` | Add all torrent files in a directory
`trrem` | Remove all finished torrents
`trclean` | Remove illegal filename characters and run `rename_files`

## File duplicates
alias | description
--- | ---
`dupe [-fr]` | Find or remove files that end with an underscore (`_`)
`dupe_md5 [-fr]` | Find or remove files with their calculated `md5` checksum
