# bspwm

## misc
config | description
--- | ---
`xset s off & xset -dmps` | Disable screen saver

## launch scripts
`config` | description
--- | ---
`sxhkd &` | Launch `sxhdk` keybinds
`set/export TZ [timezone]` | Set time zone for `polybar`
`~/.config/polybar/launch.sh` | Launch `polybar`

## monitor setup
```sh
lap="eDP-1"
mon="HDMI-1"
setup_monitors () {
  if [ "$(bspc query -M | wc -l)" -eq "2" ]; then
    bspc monitor "$lap" -d 1 2 3 4 5
    bspc monitor "$mon" -d 6 7 8 9 0
    bspc monitor "$lap" -s HDMI-1
    xrandr --output "$mon" --left-of "$lap"

    # Set wallpaper
    feh --bg-tile "$HOME/pic/wals/tiled/stripe.png"
  else
    bspc monitor "$lap" -d 1 2 3 4 5 6 7 8 9 0
  fi
}
setup_monitors
```

## window config
config | description | value
--- | --- | ---
`bspc config normal_border_color` | Border color for unfocused windows | `“#32302f”`
`bspc config focused_border_color` | Border color for focused windows | `“#458588”`
`bspc config urgent_border_color` | Border color for urgent windows | `“#cc241d”`
`bspc config border_width` | Border width | `2`
`bspc config window_gap` | Gap between windows | `12`
`bspc config split_ratio` | Ratio for window splitting | `0.50`
`bspc config borderless_monocle` | Set if monocle window should be borderless | `true`
`bspc config gapless_monocle` | Set if monocle window should be gapless | `true`

## launch programs & window rules
config | description
--- | ---
`bspc rule -a qutebrowser desktop='^6'` | Fix `qutebrowser` on workspace 6
`bspc rule -a KeePassXC desktop='^5'` | Fix `keepassxc` on workspace 5
`bspc rule -a Zathura state=tiled` | Start `zathura` in tiled mode (defaults to floating)
