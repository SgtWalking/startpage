# polybar

## modules
position | module
--- | ---
`left` | bspwm
`center` | mpd
`right` | pulseaudio wlan memory battery weather date powermenu

## modules “in-depth”
module | description
--- | ---
`bspwm` | Set names/icons for focused, open and empty workspaces
`mpd` | Set `mpd` host, port and format
`weather` | Display current weather condition and temperature
`powermenu` | `custom/menu` for locking, logging out, rebooting and shutting the laptop off
