# scripts

script | description
--- | ---
`battery.sh` | Get battery status for `ì3status`
`cmus.sh` | Get cmus song and artist for `i3status`
`fetch` | Display system info
`i3lock.sh` | Lock `i3` with `i3lock` and a screenshot as the background
`pic2pdf.sh` | Get all images in a directory and write them into a `.pdf` file
`pw_gen.sh` | Generate a password using `/dev/urandom`
`screens.sh` | Set up dual monitor setup (rotate and position screens, set wallpapers)
`weather.sh` | Weather info for `polybar`
