# sxhkd

## WM independent keybinds
config | runs | description
--- | --- | ---
`super + Return` | `xst -f "Terminus:antialias=false"` | Open a terminal
`super + d` | `rofi -show run` | Run `rofi`
`super + Escape` | `pkill -USR1 -x sxhkd` | Reload `sxhkd` and its config file

## bspwm keybinds
config | runs | description
--- | --- | ---
`super + alt + {q,r}` | `bspc {quit,wm -r}` | Quit or reload `bspwm`
`super + shift + q` | `bspc node -c` | Close focused window

## window states/flags
config | runs | description
--- | --- | ---
`super + {t,shift + t,s,f}` | `bspc node -t {tiled,pseudo_tiled,floating,fullscreen}` | Set window state
`super + ctrl + {m,x,y,z}` | `bspc node -g {marked,locked,sticky,private}` | Set window flag

## window focus and movement
config | runs | description
--- | --- | ---
`super + {_,shift +}{Left,Down,Up,Right}` | `bspc node -{f,s} {west,south,north,east}` | Move focus to window or move window
`super + {_,shift +}{1-9,0}` | `bspc {desktop -f, node -d} '^{1-9,10}'` | Move window to workspace

## media keys
config | runs | description
--- | --- | ---
`XF86Audio{Raise,Lower}Volume` | `pactl set-sink-volume @DEFAULT_SINK@ {+5%,-5%}` | Raise or lower volume
`XF86AudioMute` | `amixer -q set Master toggle` | Toggle audio
`XF86MonBrightness{Up,Down}` | `light -{A,U} 10` | Raise or lower brightness

## misc/custom
config | runs | description
--- | --- | ---
`super + shift + s` | `scrot "$HOME"/pic/scrots/scrot.png` | Take a screenshot using `scrot`
`super + shift + d` | `/bin/sh "$HOME"/doc/scripts/fa_icons` | Launch `Font Awesome rofi` menu
`super + alt + p` | `/bin/sh "$HOME"/.config/polybar/launch.sh` | Launch `polybar`
`super + shift + alt + p` | `pkill polybar` | End all running `polybar` instances
`super + x` | `/bin/sh "$HOME"/doc/scripts/powermenu` | `rofi` powermenu
